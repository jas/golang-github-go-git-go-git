golang-github-go-git-go-git (5.13.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 5.13.2
    - Fix CVE-2025-21613.  Closes: #1092678.
    - Fix CVE-2025-21614.  Closes: #1092679.
  * Fix nocheck B-D.
  * Fix lintian update-debian-copyright.
  * Add Breaks:gitsign<0.12.0-4.

 -- Simon Josefsson <simon@josefsson.org>  Thu, 13 Feb 2025 22:30:11 +0100

golang-github-go-git-go-git (5.12.0-1) unstable; urgency=medium

  [ Simon Josefsson ]
  * Team upload.
  * New upstream version 5.12.0

  [ Chris Lamb ]
  * Make it build reproducibly.  Closes: #1000401.

 -- Simon Josefsson <simon@josefsson.org>  Wed, 04 Dec 2024 21:13:09 +0100

golang-github-go-git-go-git (5.11.0-4) unstable; urgency=medium

  * Team upload

  [ Anshul Singh ]
  * unset no_proxy env to fix tests on Ubuntu autopkgtest infra.

 -- Shengjing Zhu <zhsj@debian.org>  Tue, 20 Aug 2024 15:04:07 +0800

golang-github-go-git-go-git (5.11.0-3) unstable; urgency=medium

  * Team upload.
  * Disable another test that requires internet
  * Add patch to use explicit loopback IP instead of localhost

 -- Maytham Alsudany <maytha8thedev@gmail.com>  Mon, 10 Jun 2024 17:19:43 +0800

golang-github-go-git-go-git (5.11.0-2) unstable; urgency=medium

  * Team upload.
  * Revert symlinking github.com/imdario/mergo to dario.cat/mergo
  * Bump Standards-Version to 4.7.0 (no changes)

 -- Maytham Alsudany <maytha8thedev@gmail.com>  Tue, 04 Jun 2024 15:14:28 +0800

golang-github-go-git-go-git (5.11.0-1) unstable; urgency=medium

  * Team upload.

  [ Pirate Praveen ]
  * New upstream release 5.11.0 (Fixes: CVE-2023-49568, CVE-2023-49569)
    (Closes: #1060701)
  * Bump Standards-Version to 4.6.2 (no changes needed)

  [ Maytham Alsudany ]
  * Symlink github.com/imdario/mergo to new import path (dario.cat/mergo)
  * Update (B-)Deps, add support for nocheck builds
  * Use fake home directory containing test data during tests
  * Disable more tests that require internet
  * Disable gitignore test with unreliable tilde expansion

 -- Maytham Alsudany <maytha8thedev@gmail.com>  Tue, 16 Apr 2024 15:37:15 +0300

golang-github-go-git-go-git (5.4.2-4) unstable; urgency=medium

  * Team upload.
  * Add tzdata to Build-Depends. Closes: #1027907.

 -- Santiago Vila <sanvila@debian.org>  Tue, 31 Oct 2023 23:45:00 +0100

golang-github-go-git-go-git (5.4.2-3) unstable; urgency=medium

  * Source only upload for migration to testing

 -- Pirate Praveen <praveen@debian.org>  Mon, 22 Nov 2021 10:07:32 +0530

golang-github-go-git-go-git (5.4.2-2) unstable; urgency=medium

  * Rebuild for unique version in DAK (-1 was REJECTED)

 -- Pirate Praveen <praveen@debian.org>  Fri, 19 Nov 2021 18:22:58 +0530

golang-github-go-git-go-git (5.4.2-1) unstable; urgency=medium

  * Initial release (Closes: #998884)

 -- Pirate Praveen <praveen@debian.org>  Tue, 09 Nov 2021 17:33:39 +0530
